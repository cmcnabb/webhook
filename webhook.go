package webhook

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/beeker1121/goque"
)

// Webhook - Everything we need to know about the webhook and associated queue
type Webhook struct {
	Queue    *goque.Queue
	Client   *http.Client
	URL      string
	Channel  string
	UserName string
	RLIF     bool
	RLExpire time.Time
}

// Message to send to Slack's Incoming WebHook API.
type Message struct {
	Text        string        `json:"text"`
	Channel     string        `json:"channel,omitempty"`
	UserName    string        `json:"username,omitempty"`
	IconURL     string        `json:"icon_url,omitempty"`
	IconEmoji   string        `json:"icon_emoji,omitempty"`
	Attachments []*Attachment `json:"attachments,omitempty"`
}

// Attachment provides rich-formatting to messages
type Attachment struct {
	Fallback   string  `json:"fallback,omitempty"` // plain text summary
	Color      string  `json:"color,omitempty"`    // {good|warning|danger|hex}
	AuthorName string  `json:"author_name,omitempty"`
	AuthorLink string  `json:"author_link,omitempty"`
	AuthorIcon string  `json:"author_icon,omitempty"`
	Title      string  `json:"title,omitempty"` // larger, bold text at top of attachment
	TitleLink  string  `json:"title_link,omitempty"`
	Text       string  `json:"text,omitempty"`
	Fields     []Field `json:"fields,omitempty"`
	ImageURL   string  `json:"image_url,omitempty"`
	ThumbURL   string  `json:"thumb_url,omitempty"`
	Footer     string  `json:"footer,omitempty"`
	FooterIcon string  `json:"footer_icon,omitempty"`
	Timestamp  int     `json:"ts,omitempty"` // Unix timestamp
}

// Field defines an attachment field
type Field struct {
	Title string `json:"title,omitempty"`
	Value string `json:"value,omitempty"`
	Short bool   `json:"short,omitempty"`
}

func getMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

// New  webhook and  backing queue creation
func (w Webhook) New(url string, channel string, user string) error {
	var err error
	w.Client = &http.Client{}
	w.Channel = channel
	w.UserName = user
	w.Queue, _ = goque.OpenQueue("/var/run/" + getMD5Hash(url+channel))
	w.RLIF = false
	w.RLExpire = time.Now()
	return err
}

// Send a Message to Slack
func (w Webhook) Send(m Message) (*http.Response, error) {
	if m.Channel == "" {
		m.Channel = w.Channel
	}
	if m.UserName == "" {
		m.UserName = w.UserName
	}
	buf, err := json.Marshal(m)
	if err != nil {
		return nil, err
	}
	if w.RLIF && (time.Now().After(w.RLExpire)) {
		w.RLIF = false
	}
	r, err := http.Post(w.URL, "application/json", bytes.NewReader(buf))
	if r.StatusCode == 429 {
		d, _ := strconv.Atoi(r.Header.Get("Retry-After"))
		w.RLIF = true
		w.RLExpire = time.Now().Add(time.Second * time.Duration(d))
	}
	return r, err
}

//Length Returns the number of messages remaining to be sent
func (w Webhook) Length() (l int) {
	if w.Queue.Length() > 0 {
		i, err := w.Queue.Peek()
		if err != nil {
			return 0
		}
		return len(i.ToString())
	}
	return 0
}

//RunQueue - Take messages from persistant queue and send them to slack being mindful of rate-limits
func (w Webhook) RunQueue() {
	var m Message
	for {
		if w.Queue.Length() > 0 {
			d, _ := w.Queue.Dequeue()
			m.Text = d.ToString()
			for {
				r, err := w.Send(m)
				if r.StatusCode == 429 {
					time.Sleep(time.Now().Sub(w.RLExpire))
				}
				if (err == nil) || (w.RLIF == false) {
					break
				}
			}
		}
	}
}

//Close - Close the Webhook Queue
func (w Webhook) Close() {
	w.Queue.Close()
}
