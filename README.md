A Go library to send messages to a slack webhook, with queuing to mitigate slack rate limiting
